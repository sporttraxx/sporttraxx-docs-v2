---
id: craddle
title: Sporttraxx craddle
sidebar_label: Craddle 🚧
custom_edit_url: null
---

:::caution

Work in progress!

:::

<img
src={require('/img/devices/craddle.png').default}
alt="Craddle"
class="hardware-image vertical-hardware"
/>

## Download

[Instructions](/documents/craddle.pdf)
