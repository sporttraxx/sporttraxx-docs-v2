---
id: checklist
title: Check list
sidebar_label: Check list
custom_edit_url: null
---

This page allows to create a list with race number, competitors name, assigned tracker ID and note fields. It can be printed from the browser and used to manage notes during the event.

![CheckList Page](/img/screenshots/checklist.png)
