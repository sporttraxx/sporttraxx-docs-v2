---
id: sop
title: "Sporttraxx Operating Protocol (SOP)"
sidebar_label: Operating Protocol
custom_edit_url: null
---

The Operating protocol is the sequence of actions that a Sporttraxx technician is following when running a tracking service at an event. A list of these actions in their proper order is available and should be used as a checklist.

Only the respect of these actions will allow the [SPI (Sporttraxx Performance Indicator)](spi) to
provide a likely statistics to be used as a quality target and a proof of service quality.

## In the days before the event

Collect/set up the GPS files of the event’s route, in .kml format with the following specifications:

#### Rallies and offroad motorsports

- Start/finish points
- Special Stages
- Road sections
- Radio points
- Check points
- Split times points
- Service/regrouping points

#### Horse Endurance

- Start/finish points
- Phases
- Check points

#### Other sports

- Start/finish points
- Routes
- Checkpoints and any other relevant point

If a GSM test was run earlier, produce a screenshot, using the [TrackAnalysis 2](trackanalysis2), that shows
the different levels of GPRS coverage along the race route by highlighting in red the critical
areas. That document is useful, during the event, to better displace manpower in critical areas with poor or no coverage.


## The day before the event

After the assignment of the trackers to the competitor’s numbers and the service vehicle’s
acronyms, turn on all the trackers in an open space to check the GPRS connection, the GPS
positions and the actual battery levels of the trackers. Turn off the trackers from remote. Check visually that
they are all OFF. Charge those trackers that were showing a lower battery level.
Prepare the TP link by uploading stages and relevant points in the proper order.
Prepare the order link with the expected starting order

## The day of the event

Choose the most convenient location for the distribution/installation of the trackers.
Turn on the trackers in the starting order sequence, at least 15 minutes before the actual start of
each competitor. Using a mobile phone/tablet check the connection status of each tracker.
The connection can be eventually checked relying on the tracker’s LED flashing mode. Restart those trackers which do not come online. After the third attempt, proceed with swapping the critical tracker(s) with a spare device, updating immediately the database or informing the operator in race control.
Sign as **OUT** those trackers that will not take the start, only if based upon a reliable information.
Switch off the trackers of the competitors that have been marked as **OUT**.

**Important**: the [SPI (Sporttraxx Performance Indicator)](spi) is continuosly updated and is based on the average connection status, during the whole event, of all the active trackers assigned to competitors. Therefore the highest
attention must be paid in marking asap as **OUT** any non-starter and any retired competitor, as soon as that retired status is known.
The SPI will not consider the **OUT** marked competitors.

## During the event

### Monitoring the connections

There are several tools in the Sporttraxx platform, that allow to monitor the connection status of
the trackers.

**1)** In the [Overview](overview), the DT column shows the **delay since the last transmission**. When
moving, a tracker should send a position each 5 to 7 seconds. When stopped, it also stops the
transmissions till the next movement. That is meant to save battery and reduce the data flow.
If the **auto req. Pos.** option has been activated, that PC will poll each 4 minutes all the
trackers, forcing also the stopped ones and the ones in sleep mode, to confirm their actual position. That option might affect significantly the battery consumption. A reload of the overview page on that PC, will reset the **auto req. Pos.**
A missing **Battery level** icon might warn about a disconnected tracker. Check the DT of
that tracker. If it was last moving and the delay is > 20 seconds, it might predict a
disconnection. If it was last not moving, it is normal to not have new updates of position.
Since the battery level is updated each 5 minutes, a missing battery level might be related to a
single missed transmission of that level. With the next update the battery level will be back. Also a
sudden abnormal value of battery level (sudden unexpected red low level) can be related to a wrong reading
of the battery tension. With the following update, the level should go back to the previous levels.

You can use manually the **highlight disconnected** command. In front of each tracker’s line, a green (connected) or a red (disconnected) dot is shown, till the next refresh of the page.

**2)** In the [Overview 2](overview2), the colour of the device ID shows all the time the connection status,
without the need for a specific request: red for **disconnected**, green for **online**. With a DT value
shown in the next column.
In the upper right corner the **SPI** is shown as a % value, updated every minute.

**3)** In the [Map2d](map2d), the **Toggle connection info** command in the menu, allows to see the actual connection status of all trackers on the map. Green = connected, Red = disconnected from the network

### Time out

Before the system is showing a tracker as disconnected, a timeout of a few minutes will run. That will avoid to show as disconnected a tracker that is only temporarily “suffering” in a difficult area. After that timeout, the tracker will be considered as disconnected (red).

### Management of the trackers

In the overview page, after a correct start procedure, you should have listed ALL the trackers that are used on that day. You should hide the trackers that were not installed/handed out, that are for sure in your hands and OFF. During the day keep that list always updated so that, at the end of the day, when it comes to the collection of the trackers, you work on a reliable and complete list of trackers that you are supposed to collect.

### Issues

In the [Issues](issues), a useful tool to record problems rised during an event, can help in updating the software and create a kind of ranking for the tracking devices. The information collected during the event should be evaluated immediately after the end of the event and eventually transferred to the notes related to the devices.

## End of the event

The procedures to be followed at the end of the event and the collection of the trackers depend
significantly from the organization and the logistics on site.

**Our suggestion**:
Each tracker, as soon as collected, should be reported as collected using the [Manage](manage) page. That adds the
blue status icon in the overview page to that tracker. After that, turn off the device. That
procedures will show both the **COLLECTED** icon and the **OFF** icon in the overview page.

As an alternative, not having a smartphone or tablet available in order to use the [Manage](manage) page, you can
press the **SOS** button before turning off the tracker. That creates the combination of SOS icon
and OFF icon in the overview. A reliable information that the tracker is in your hands.

[SOP](spi)

Following these actions will allow the [SPI (Sporttraxx Performance Indicator)](spi) to
provide a likely statistics and be used as a quality target.
