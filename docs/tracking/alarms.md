---
id: alarms
title: Alarms
sidebar_label: Alarms
custom_edit_url: null
---

This page allows the management of the **SOS**/**OK** incoming messages and the transmission of text messages to the tracking devices.

![Alarms Page](/img/screenshots/alarms.png)

Incoming messages are listed in arrival order showing time and date, number and name of sender, type of alarm and coordinates. Clicking on them opens the /map2d page centered on the alarm's position.

## Functions

### Activate notifications

It activates an automatic popup message and acoustic warning on the user's machine. **Note**: not all browsers support this function.

### Write msg

It opens a box where text messages can be edited and addressed to trackers that can manage text messages.
