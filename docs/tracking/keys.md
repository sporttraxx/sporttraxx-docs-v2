---
id: keys
title: Keys
sidebar_label: Keys
custom_edit_url: null
---

This page allows the management and distribution of keys for the [Tracky App](https://tracky-docs.sporttraxx.net/).

![Keys Page](/img/screenshots/keys.png)

## Functions

### Edit

Links to vehicle/keys administration page.

### Print

Creates a printable list of keys and QR codes for easier distribution. Use the browser's printing settings.

![Keys QR Codes Page](/img/screenshots/keys-qr-codes.png)

### Download CSV

Creates a downloadable .csv file of the competitors/keys association. Tipically for an electronic distribution

### Toggle keys

Shows/hides the keys in the main page.  To keep the keys confidential in front of queuing people.

## Key details

By clicking on a key in the main page list, a subpage with the details for that specific key will be displayed.

![App Key](/img/screenshots/app-key.png)
