---
id: map2d
title: Map2D
sidebar_label: Map2D
custom_edit_url: null
---

This page shows the live-tracking on different maps and allows some basic interactions with the tracking devices.

![Map2D Page](/img/screenshots/map2d.png)

## Search bar

### Menu

Clicking on the menu icon in the search box opens a side menu with these options:

- _**Map Type**_: Allows the selection of different cartographic and satellite maps. It includes an empty map to print or send screenshots of reports with reduced file size
- _**Other settings**_:
  - **Toggle places** shows on the map the actual locations of [Sporttraxx clocks](../hardware/clock) highlighting their “should be” position. That helps detecting wrong placements of timing equipment on the field.
  - **Toggle ruler** opens a ruler tool that allows  measurements between points and heading indication (CAP), for quick helicopter interventions. 
  - **Toggle connection info** switches the color of the icons from movement to connection status (green = connected, red = disconnected).
  - **Toggle speed color** switches the color of the icons to speed related. The threshold is set after selecting this option. 
  - **Toggle all tracks** shows/removes a tail for all competitors from that moment onwards
  - **Hide competitors** hides all competitors from the map
  - **Hide service vehicles** hides all service vehicles from the map
  - **Show hidden vehicles** allows to see on that PC only, icons with a NON_PUBLIC status
- _**Links**_:
  - **Overview** Links back to the [Overview](overview) page.
  - **Login/Logout**

### Search field

To target a competitor and center it the map, type its number or name in the search field.
A competitor's box will be opened on the bottom of the map. Same can be accomplished by clicking on the competitor's icon in the map.

Only vehicles shown on the map can be found with the search bar. If they are in NON PUBLIC, COLLECTED and HIDDEN status, they are not suggested in the search field, and not shown on the map.

## Icons

Icons can have two different **shapes**:

- <span class="outer-shape outer-dot"><span class="shape dot"><span class="second-shape second-dot moving"><span class="number">101</span></span></span></span> Round shaped for competitors with racing number
- <span class="outer-shape outer-square"><span class="shape square"><span class="second-shape second-square moving"><span class="number">101</span></span></span></span> Square shaped for vehicles with no racing number. Service vehicles are not visible to users who are not logged in with a password.

Vehicles set as NON PUBLIC, COLLECTED and HIDDEN are not shown on the map

A competitor's icon is essential in understanding its current state:

- <span class="outer-shape outer-dot"><span class="shape dot"><span class="second-shape second-dot moving"><span class="number">101</span></span></span></span> **Moving** (speed > 7 km/h)
- <span class="outer-shape outer-dot"><span class="shape dot"><span class="second-shape second-dot not_moving"><span class="number">101</span></span></span></span> **Stopped** (blue ripple animation)
- <span class="outer-shape outer-dot"><span class="shape dot"><span class="second-shape second-dot ok"><span class="number">101</span></span></span></span> **OK** button was pressed
- <span class="outer-shape outer-dot"><span class="shape dot"><span class="second-shape second-dot sos"><span class="number">101</span></span></span></span> **SOS** button was pressed (jumping animation)
- <span class="outer-shape outer-dot"><span class="emoji">🔥</span><span class="shape dot"><span class="second-shape second-dot sos"><span class="number">101</span></span></span></span> **Fire SOS** button was pressed (jumping animation)
- <span class="outer-shape outer-dot"><span class="shape dot"><span class="second-shape second-dot road_blocked"><span class="number">101</span></span></span></span> **Road blocked/Hazard** button was pressed
- <span class="outer-shape outer-dot"><span class="shape dot"><span class="second-shape second-dot crash"><span class="number">101</span></span></span></span> A **crash** has been detected
- <span class="outer-shape outer-dot"><span class="retired">R</span><span class="shape dot"><span class="second-shape second-dot moving"><span class="number out_text">101</span></span></span></span> **Retired**
- <span class="outer-shape outer-dot"><img
  src={require('/img/redflag/redflag_sent.png').default}
  alt="Example banner"
  class="redflag-small-flag"
  /><span class="shape dot"><span class="second-shape second-dot moving"><span class="number">101</span></span></span></span> Redflag **sent** to the device
- <span class="outer-shape outer-dot"><img
  src={require('/img/redflag/redflag_rec.png').default}
  alt="Example banner"
  class="redflag-small-flag"
  /><span class="shape dot"><span class="second-shape second-dot moving"><span class="number">101</span></span></span></span> Redflag **received** by the device
- <span class="outer-shape outer-dot"><img
  src={require('/img/redflag/redflag_ack.png').default}
  alt="Example banner"
  class="redflag-small-flag"
  /><span class="shape dot"><span class="second-shape second-dot moving"><span class="number">101</span></span></span></span> Redflag **acknowledged** by the crew

Icon’s **transparency** may be:

- <span class="outer-shape outer-dot"><span class="shape dot"><span class="second-shape second-dot moving"><span class="number">101</span></span></span></span> Bright color as long as the position is updated (not older than 20 seconds)
- <span class="outer-shape outer-dot"><span class="shape dot"><span class="second-shape second-dot moving-faded"><span class="number">101</span></span></span></span> Gradually fading when position is becoming old (older than 20 seconds)

**Note**: make sure the time of your PC is synchronized with the GPS/internet time as the level of fading can be affected by a wrong synchronization.

Depending on specific settings of the trackers, if not moving and therefore not transmitting, the transparency level might stay faded till a manual request position command.  
If the **Auto. rec. pos.** option has been selected in the Overview page, then all devices will be polled once every four minutes. That keeps the level of transparency below certain values.


**Note**: too many forced transmissions (request position commands sent) might affect the battery duration of the devices.

## Competitor's box

![Bottom box](/img/screenshots/bottombar.png)
Displayed information are:

- _**Custom color**_ The color assigned to the circle around the icon. Can be changed from here by clicking on it
- _**Number and name of competitor/service vehicle**_
- _**Actual speed**_
- _**GPS validity**_ Moving the pointer on it will show actual coordinates that can be copied in the clipboard
- _**Age of last transmitted position**_
- _**NMV flag** A purple box shows a totally not-moving tracker. (movement sensor).

### Command buttons

- _**Toggle track**_: show/remove a tail with the last 300 transmitted positions
- _**Request position**_: request an update of the position. As a consequence of a NMV flag, requesting a position is forcing the tracker to confirm again its NMV status.
- _**Req. Call/ACK**_: a callback request or an acknowledgment to an SOS message can be sent. **On GL300/310/320 only**, also a vibration is triggered. On FIA and UK trackers it triggers the green LED.
- _**Stop time**_: show the time a vehicle stopped in its current location and for how long

### Checkboxes

- _**SOS**_: shows a received SOS request. Can be reset by clicking on it
- _**OK**_: shows a received OK info. Can be reset by clicking on it
- _**NEW_ALARM**_: disables the animation of a new alarm
- _**OUT**_: activates the OUT status, By checking it, the icon will be marked with an "R" and racing number is barred = out of race
- _**NON_PUBLIC**_: by checking it, that icon will not be visible on the map anymore. It can be seen again on a pc with activated **Show hidden vehicles** option.

Clicking on a different icon will replace the previous competitor's box. The "X" will close the box.

## Hidden functions (key combinations)

- _**Switch between Google Maps and MapBox**_ :
  By typing _**Shift + s m**_ the screen will switch to a different version of the [Map2D](map2d) page with different 3D map options
- _**Show GPRS connections**_:
  By typing _**s c**_ the color of the icons will change to Green = connected or Red = disconnected
- _**Open Ruler**_:
  By typing _**r**_ a ruler will be opened or closed on the map
- _**Open Menu**_:
  By typing _**m**_ or _**.**_ the special settings menu will be opened or closed on the map
- _**Toggle Track All**_:
  By typing _**t t a**_ all the icons will leave a colored tail on the map, from that moment onwards
- _**Show IMEI**_:
  By typing _**s i**_ all the icons will show the IMEI that last transmitted that position. Useful when using alias trackers which are transmitting in parallel mode

## Special import option

You can anytime temporarily overlay NMEA or KML files into the map2d link by dragging them on the map in that pc only.
