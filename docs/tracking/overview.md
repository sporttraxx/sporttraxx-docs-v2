---
id: overview
title: Overview
sidebar_label: Overview
custom_edit_url: null
---

import Inbox from '@site/static/img/inbox.svg'

This page allows the management of the tracking devices, monitoring their movement, speed, connection status, GPS position, battery level and any possible status the device might be in.
It also makes it possible to customize the [Map2D](map2d) icons with colored frames and activate the auto/request position option.

![Overview Page](/img/screenshots/overview.png)

## Table

- _**GPRS Connection**_ status (upon request, using the function _Highlight Disconnected_ in the right sidebar)
- _**Checkbox**_ to select the competitor
- _**Colorbox**_ to change the color of the frame of the competitor's icon on the [Map2D](map2d) page
- _**#**_ race number assigned to the competitor
- _**Name**_: competitor’s name
- _**Device**_: (main) tracking device ID assigned to the competitor
- _**Pos.**_: position validity (✓ or X)
- _**Speed**_: current speed of the tracker
- _**dt**_: time since the last transmitted position
- _**st**_: status of the tracker
- _**Bat**_: level of the tracking device's battery (additional battery boxes are related to aliases)

### Device statuses

- <span class="badge off_badge">off</span> The device has been turned off manually or remotely.
- <span class="badge collected_badge"><Inbox className="inbox" /></span> Sporttraxx has collected the device.
- <span class="badge sos_badge">SOS</span> The competitor sent a MEDICAL SOS alarm.
- <span class="badge sos_badge">SOS_FIRE</span> The competitor sent a Fire SOS alarm.
- <span class="badge road_blocked_badge">ROAD BLOCKED</span> The competitor sent a ROAD BLOCK/HAZARD alarm.
- <span class="badge crash_badge">CRASH</span> a Crash has been detected via the accelerometer.
- <span class="badge ok_badge">OK</span> The competitor sent an OK message.
- <span class="badge nmv_badge">NMV</span> The tracker does not detect any movement.
- <span class="badge rf_sent_badge">redflag:sent</span> Redflag sent.
- <span class="badge rf_recv_badge">redflag:recv</span> Redflag received.
- <span class="badge rf_ack_badge">redflag:ack</span> Redflag acknowledged.
- <span class="badge out_badge">out</span> The competitor has retired.
- <span class="badge lowbat_badge">lowbat</span> The battery level is critically low.

Each column can be sorted by clicking on its name on the top.  
If alias trackers are used, an additional battery column will be automatically displayed.  
The overview page will automatically be filled with lines and competitor’s details, as soon as they connect to GPRS for the first time.  
Lines shown in faded grey are vehicles put in NON_PUBLIC mode in the [Map2D](map2d) page, in order to keep them not visible on the map.

When moving the cursor on the _**Pos.**_ cell, the actual coordinates of the last transmission are shown and the tracker's IMEI. By clicking on it, a link to Google Maps will be opened and the GPS coordinates will be available in 2 more formats.

## Right sidebar

### Auto req.pos. button

If activated, the browser will send a request position command to all trackers once every 4 minutes.  
**Note**: this option could significantly affect the battery duration of the trackers, forcing them to transmit even if they are stationary or in sleep mode. Use carefully

### Filters

Hide devices in the table according to their status.

### Actions

- _**Refresh**_: manual refresh of the table. Does not affect the sorted sequence in the columns
- _**Highlight Disconnected**_: shows GPRS connection status by means of a green/red dot in the very first column
- _**Copy devices**_: copies selected device(s) IDs in the computer's clipboard. For external softwares
- _**Request position**_: sends a position request to a selected device(s)
- _**Request I/O**_: sends a request of status of connected accessories
- _**Toggle collected**_: assigns a _collected_ status to the selected device
- _**Reset states**_: resets the status(es) of the selected device. To be used carefully.
- _**Hide**_: hides the selected device from the list and in the [Map2D](map2d) page until its next transmission (if any). Hidden status can be cancelled in the map link, if a transmission is missing.
- _**Reset**_: sends a GSM + GPS reset to the selected device
- _**Power off**_: turns off the selected device. **Note**: if turned off, the device cannot be reached again

Commands sent through this link are mainly addressed to the main device (for aliases use the [Overview2](overview2))

### Links

- _**Devices**_: link to the [Devices](connections) page that shows the connection status of all trackers of the event.
- _**Map**_: link to the [Map](map) page that allows manual intervention on the map position of the tracking devices.
