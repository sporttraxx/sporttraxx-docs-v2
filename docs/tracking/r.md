---
id: r
title: "Start page"
sidebar_label: Start page
custom_edit_url: null
---

This page allows the direct access to all the events assigned to the _Team_ you are part of. For each event you can access to what you are allowed to, according to your credentials.

![r Page](/img/screenshots/r.png)

## Description

By clicking on the event’s name you access the Map2d page.

Through the event’s icons, you can directly access the most relevant tools:

- [Overview](overview)
- [Map2D](map2d)
- [TP](tp)
- [Track Analysis](trackanalysis2)
- [Checklist](checklist)
- [Timing](timing)
