---
id: spi
title: "Sporttraxx Performance Indicator (SPI)"
sidebar_label: Performance Indicator
custom_edit_url: null
---

The Performance Indicator is the tool which allows the evaluation of the overall quality of
the Sporttraxx tracking service during an event.
It is based on the constant monitoring of the connection status of all the
trackers associated to competitors only. Retired competitors (out), service vehicles and
spare units are excluded from this analysis. Therefore pay great attention to keep the status of the monitored vehicles up to date. Once a competitor is know to have withdrawn, set him as OUT. Once a tracker has been collected, turn it off and hide it in the overview list. These actions, all together, keep the SPI at highest and most realistic level.

The reliability of the generated data is based on the strict respect of the [Sporttraxx
Operating Protocol](sop).

Specific daily SPI diagrams are available from a dedicated software.
The current SPI is visible in the [Overview2](overview2).
